## california_roads ##

Average clustering coefficient      0.0464  
Diameter (longest shortest path)    849  
90-percentile effective diameter    500  
average nbr of edges per node       2.821

## internet_topology_2005 ##

Average clustering coefficient      0.2581  
Diameter (longest shortest path)    25  
90-percentile effective diameter    6  
average nbr of edges per node       13.093

## enron_emails ##

Average clustering coefficient      0.4970  
Diameter (longest shortest path)    11  
90-percentile effective diameter    4.8  
average nbr of edges per node       10.732

## condense_matter_physics_collaboration ##

Average clustering coefficient      0.6334  
Diameter (longest shortest path)    14  
90-percentile effective diameter    6.5  
average nbr of edges per node       8.549
