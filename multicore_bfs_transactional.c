#include "graph_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <pthread.h>

#define NBR_CHUNK_ENTRIES 50

typedef struct {
    uint32_t nbr_entries;
    uint32_t entries[NBR_CHUNK_ENTRIES];
} chunk;

uint16_t nbr_threads;
char done;

graphnode *graph;
char *visited;

chunk *frontier, *next_frontier;
uint32_t frontier_size, frontier_max_size, frontier_pos;
uint32_t next_frontier_size, next_frontier_pos;

//pthread_mutex_t mtx;

pthread_barrier_t barrier;

/*uint32_t aet_chunk(chunk **chunk_array, uint32_t *array_size, uint32_t *array_pos) {
    uint32_t ret;
    pthread_mutex_lock(&mtx);
    if (*array_size <= *array_pos) {
        *array_size *= 2;
        *chunk_array = realloc(*chunk_array, sizeof(chunk) * (*array_size));
    }
    ret = *array_pos;
    *array_pos += 1;
    pthread_mutex_unlock(&mtx);
    return ret;
}*/

uint32_t get_chunk(chunk **chunk_array, volatile uint32_t *array_size, uint32_t *array_pos) {
    uint32_t ret;
    //pthread_mutex_lock(&mtx);
    ret = __sync_fetch_and_add(array_pos, 1);
    if (ret >= *array_size) {
        if (ret == *array_size) {
            *chunk_array = realloc(*chunk_array, sizeof(chunk) * 2 * (*array_size));
            *array_size *= 2;
        }
        while (ret > *array_size);
    }
    /*if (*array_size <= *array_pos) {
        *array_size *= 2;
        *chunk_array = realloc(*chunk_array, sizeof(chunk) * (*array_size));
    }*/
    //ret = *array_pos;
    //*array_pos += 1;
    //pthread_mutex_unlock(&mtx);
    return ret;
}

void *thread_func(void *arg) {
    uint32_t crt_chunk, nf_chunk;
    uint32_t i, j, nbr_neighbors, nbr_nodes, neighbor;
    char new_chunk = 1;
    graphnode g;
    int ret;

    //struct timespec t_before;
    //struct timespec t_after;

    // while not done: get next chunk and process it
    while (!done) {
        crt_chunk = __sync_fetch_and_add(&frontier_pos, 1);
        if (crt_chunk < frontier_size) {

            nbr_nodes = frontier[crt_chunk].nbr_entries;

            for (i = 0; i < nbr_nodes; i++) {
                // Add unvisited neighbors to next_frontier
                g = graph[frontier[crt_chunk].entries[i]];
                nbr_neighbors = g.nbr_neighbors;
                for (j = 0; j < nbr_neighbors; j++) {
                    neighbor = g.neighbors[j];

                    if (visited[neighbor] == 0) {
                        char visit = 0;
                        __transaction_atomic {
                            if (visited[neighbor] == 0) {
                                visited[neighbor] = 1;
                                visit = 1;
                            }
                        }

                        if (visit) {
                            if (new_chunk == 0 && next_frontier[nf_chunk].nbr_entries < NBR_CHUNK_ENTRIES) {
                                next_frontier[nf_chunk].entries[next_frontier[nf_chunk].nbr_entries] = neighbor;
                                next_frontier[nf_chunk].nbr_entries++;
                            } else {
                                nf_chunk = get_chunk(&next_frontier, &next_frontier_size, &next_frontier_pos);
                                next_frontier[nf_chunk].nbr_entries = 1;
                                next_frontier[nf_chunk].entries[0] = neighbor;
                                new_chunk = 0;
                            }
                        }
                    }
                }
            }
        } else { // else go to barrier
            //clock_gettime(CLOCK_REALTIME, &t_before);
            ret = pthread_barrier_wait(&barrier);
            if (ret == PTHREAD_BARRIER_SERIAL_THREAD) {
                if (next_frontier_pos != 0) {
                    chunk *temp = next_frontier;
                    uint32_t temp_size = next_frontier_size;
                    uint32_t temp_pos = next_frontier_pos;
                    next_frontier = frontier;
                    next_frontier_size = frontier_max_size;
                    next_frontier_pos = 0;
                    frontier = temp;
                    frontier_max_size = temp_size;
                    frontier_size = temp_pos;
                    frontier_pos = 0;
                } else {
                    done = 1;
                }
            }
            pthread_barrier_wait(&barrier);
            new_chunk = 1;
            //clock_gettime(CLOCK_REALTIME, &t_after);
            //printf("Time: %ld s ; %ld ns\n", t_after.tv_sec - t_before.tv_sec, t_after.tv_nsec - t_before.tv_nsec);
        }
    }
    return NULL;
}

void bfs(uint32_t nbr_nodes, uint32_t root) {
    //pthread_mutex_init(&mtx, NULL);
    pthread_barrier_init(&barrier, NULL, nbr_threads);

    frontier_size = 1;
    frontier_max_size = 100;
    frontier_pos = 0;
    frontier = malloc(sizeof(chunk) * frontier_max_size);
    frontier[0].nbr_entries = 1;
    frontier[0].entries[0] = root;

    next_frontier_size = 100;
    next_frontier_pos = 0;
    next_frontier = malloc(sizeof(chunk) * next_frontier_size);

    done = 0;

    pthread_t thread_ids[nbr_threads - 1];
    uint16_t i;
    for (i = 0; i < nbr_threads - 1; i++) {
        if (pthread_create(&thread_ids[i], NULL, &thread_func, NULL) != 0) {
            printf("pthread_create failed, quitting...\n");
            exit(0);
        }
    }
    thread_func(NULL);
    for (i = 0; i < nbr_threads - 1; i++) {
        pthread_join(thread_ids[i], NULL);
    }

    pthread_barrier_destroy(&barrier);
    //pthread_mutex_destroy(&mtx);
}

int main(int argc, char **argv) {
    char *filepath = "graphs/test.txt";
    if (argc > 1) {
        filepath = argv[1];
    }

    uint32_t nbr_nodes;
    printf("Loading graph: %s\n", filepath);
    load_graph(filepath, &graph, &nbr_nodes);
    printf("Done loading graph :)\n");

    struct timespec t_before;
    struct timespec t_after;

    // set global vars
    /*nbr_threads = 8;
    visited = calloc(nbr_nodes, sizeof(char));

    // run bfs
    clock_gettime(CLOCK_REALTIME, &t_before);
    bfs(nbr_nodes, 0);
    clock_gettime(CLOCK_REALTIME, &t_after);*/

    nbr_threads = 1;
    if (argc > 2) {
        nbr_threads = atoi(argv[2]);
    }
    printf("Running bfs with %d threads\n", nbr_threads);
    visited = calloc(nbr_nodes, sizeof(char));

    uint32_t i, j;
    uint64_t exec_times[10];
    for (j = 0; j < 10; j++) {
        exec_times[j] = 0;
    }
    uint64_t mean_exec_time = 0, exec_time, temp;
    for (i = 0; i < 10; i++) {

        // run bfs
        clock_gettime(CLOCK_REALTIME, &t_before);
        bfs(nbr_nodes, 0);
        clock_gettime(CLOCK_REALTIME, &t_after);

        exec_time = (uint64_t)1000000000 * (t_after.tv_sec - t_before.tv_sec) + (t_after.tv_nsec - t_before.tv_nsec);
        mean_exec_time += exec_time;
        printf("%lu ", exec_time);

        temp = 0;
        for (j = 0; j < 10; j++) {
            if (exec_times[j] < exec_time) {
                temp = exec_times[j];
                exec_times[j] = exec_time;
                exec_time = temp;
            }
        }

        for (j = 0; j < nbr_nodes; j++) {
            visited[j] = 0;
        }
    }
    free(visited);
    printf("\n");

    mean_exec_time = mean_exec_time / 10;
    uint64_t median_exec_time = (exec_times[4] + exec_times[5]) / 2;

    printf("Mean: %lu ns\nMedian: %lu ns\nMin: %lu ns\nMax: %lu ns\n", mean_exec_time, median_exec_time, exec_times[9], exec_times[0]);

    // remove
    /*/uint32_t i, nbr_visited = 0;
    for (i = 0; i < nbr_nodes; i++) {
        if (visited[i]) {
            nbr_visited++;
        }
    }
    printf("Nbr of visited nodes: %d\n", nbr_visited);*/
    //

    //free(visited);

    printf("Time: %ld s ; %ld ns\n", t_after.tv_sec - t_before.tv_sec, t_after.tv_nsec - t_before.tv_nsec);
    free_graph(graph, nbr_nodes);
}





/*
int recursive_func(node, vector, index) {
    index = recursive_func(node.left, vector)
    vector[index] = node.value
    index = recursive_func(node.right, vector)
    return index + 1
}
*/
