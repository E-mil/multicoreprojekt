#include "graph_utils.h"

#include <stdlib.h>
#include <stdio.h>

void load_graph(char* path, graphnode **graph, uint32_t *nbr_nodes) {
    FILE *f = fopen(path, "r");

    if (f == NULL) {
        printf("Failed to load input file, quitting...\n");
        exit(1);
    }

    int ret;
    uint32_t nodes, nbr_edges;
    ret = fscanf(f, "%d %d", &nodes, &nbr_edges);

    if (ret != 2) {
        printf("Unable to read input file, quitting...\n");
        exit(1);
    }

    graphnode *_graph = malloc(sizeof(graphnode) * nodes);
    uint32_t *allocated_memory = malloc(sizeof(uint32_t) * nodes);

    allocated_memory[0] = 0;

    uint32_t i;
    for (i = 0; i < nodes; i++) {
        _graph[i].nbr_neighbors = 0;
        allocated_memory[i] = 10;
        _graph[i].neighbors = malloc(sizeof(uint32_t) * 10);
    }

    uint32_t n1, n2;
    for (i = 0; i < nbr_edges; i++) {
        ret = fscanf(f, "%d %d", &n1, &n2);
        if (ret != 2) {
            printf("Unable to read input file, quitting...\n");
            exit(1);
        }
        _graph[n1].neighbors[_graph[n1].nbr_neighbors] = n2;
        _graph[n1].nbr_neighbors++;
        if (_graph[n1].nbr_neighbors == allocated_memory[n1]) {
            allocated_memory[n1] *= 2;
            _graph[n1].neighbors = realloc(_graph[n1].neighbors, sizeof(uint32_t) * allocated_memory[n1]);
            if (_graph[n1].neighbors == NULL) {
                printf("Realloc failed, quitting...\n");
                exit(1);
            }
        }
    }

    free(allocated_memory);
    fclose(f);
    *nbr_nodes = nodes;
    *graph = _graph;
}

void free_graph(graphnode *graph, uint32_t nbr_nodes) {
    uint32_t i;
    for (i = 0; i < nbr_nodes; i++) {
        free(graph[i].neighbors);
    }
    free(graph);
}