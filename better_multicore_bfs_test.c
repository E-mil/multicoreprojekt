#include "graph_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <pthread.h>

typedef struct llnode {
    uint32_t item;
    struct llnode* next;
} llnode;

llnode *frontier_head;
llnode *next_frontier_head, *next_frontier_tail;

graphnode *graph;
char *visited;

uint16_t nbr_threads;

pthread_mutex_t frontier_mtx, next_frontier_mtx, visited_mtx;
pthread_barrier_t p_barrier;
char done;

void *thread_func(void *arg) {
    llnode *crt_node;
    llnode *local_head = NULL, *local_tail;
    uint32_t i, nbr_neighbors;

    // while not done: get next node and process it
    while (!done) {
        pthread_mutex_lock(&frontier_mtx);
        if (frontier_head != NULL) { // if it exists, get next node in frontier
            crt_node = frontier_head;
            frontier_head = frontier_head->next;
            pthread_mutex_unlock(&frontier_mtx);

            // Add unvisited neighbors to next_frontier
            graphnode g = graph[crt_node->item];
            nbr_neighbors = g.nbr_neighbors;
            for (i = 0; i < nbr_neighbors; i++) {
                uint32_t neighbor = g.neighbors[i];
                /*pthread_mutex_lock(&visited_mtx);
                if (!visited[neighbor]) { // if neighbor not visited
                    visited[neighbor] = 1; // set as visited
                    pthread_mutex_unlock(&visited_mtx);*/
                if (__sync_bool_compare_and_swap(&visited[neighbor], 0, 1)) {

                    llnode *n = malloc(sizeof(llnode)); // create llnode for neighbor
                    n->next = NULL;
                    n->item = neighbor;

                    //pthread_mutex_lock(&next_frontier_mtx);
                    if (local_head == NULL) {
                        local_head = n;
                        local_tail = n;
                    } else {
                        local_tail->next = n;
                        local_tail = n;
                    }
                    //pthread_mutex_unlock(&next_frontier_mtx);
                }
            }

            if (local_head != NULL) {
                pthread_mutex_lock(&next_frontier_mtx);
                if (next_frontier_head == NULL) {
                    next_frontier_head = local_head;
                    next_frontier_tail = local_tail;
                } else {
                    next_frontier_tail->next = local_head;
                    next_frontier_tail = local_tail;
                }
                pthread_mutex_unlock(&next_frontier_mtx);
                local_head = NULL;
            }

            free(crt_node); // done processing current node
        } else { // else go to barrier
            pthread_mutex_unlock(&frontier_mtx);
            //barrier_block();
            int ret = pthread_barrier_wait(&p_barrier);
            if (ret == PTHREAD_BARRIER_SERIAL_THREAD) {
                if (next_frontier_head != NULL) {
                    frontier_head = next_frontier_head;
                    next_frontier_head = NULL;
                } else {
                    done = 1;
                }
            }
            pthread_barrier_wait(&p_barrier);
        }
    }
    return NULL;
}

void bfs(uint32_t nbr_nodes, uint32_t root) {

    frontier_head = malloc(sizeof(llnode));
    frontier_head->item = root;
    frontier_head->next = NULL;
    next_frontier_head = NULL;

    pthread_mutex_init(&frontier_mtx, NULL);
    pthread_mutex_init(&next_frontier_mtx, NULL);
    //pthread_mutex_init(&visited_mtx, NULL);

    pthread_barrier_init(&p_barrier, NULL, nbr_threads);

    //barrier = 0;
    //barrier_open = 0;
    done = 0;

    pthread_t thread_ids[nbr_threads - 1];
    uint16_t i;
    for (i = 0; i < nbr_threads - 1; i++) {
        if (pthread_create(&thread_ids[i], NULL, &thread_func, NULL) != 0) {
            printf("pthread_create failed, quitting...\n");
            exit(0);
        }
    }
    thread_func(NULL);
    for (i = 0; i < nbr_threads - 1; i++) {
        pthread_join(thread_ids[i], NULL);
    }

    pthread_barrier_destroy(&p_barrier);
    pthread_mutex_destroy(&frontier_mtx);
    pthread_mutex_destroy(&next_frontier_mtx);
    //pthread_mutex_destroy(&visited_mtx);
}

int main(int argc, char **argv) {
    char *filepath = "graphs/test.txt";
    if (argc > 1) {
        filepath = argv[1];
    }

    uint32_t nbr_nodes;
    printf("Loading graph: %s\n", filepath);
    load_graph(filepath, &graph, &nbr_nodes);
    printf("Done loading graph :)\n");

    struct timespec t_before;
    struct timespec t_after;

    // set global vars
    nbr_threads = 4;
    visited = calloc(nbr_nodes, sizeof(char));

    // run bfs
    clock_gettime(CLOCK_REALTIME, &t_before);
    bfs(nbr_nodes, 0);
    clock_gettime(CLOCK_REALTIME, &t_after);

    // remove
    uint32_t i, nbr_visited = 0;
    for (i = 0; i < nbr_nodes; i++) {
        if (visited[i]) {
            nbr_visited++;
        }
    }
    printf("Nbr of visited nodes: %d\n", nbr_visited);
    //

    free(visited);

    printf("Time: %ld s ; %ld ns\n", t_after.tv_sec - t_before.tv_sec, t_after.tv_nsec - t_before.tv_nsec);
    free_graph(graph, nbr_nodes);
}
