#include "graph_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <pthread.h>

char *visited;
graphnode *graph;

uint32_t *frontier;
uint32_t frontier_size, frontier_pos, frontier_max_size;
uint32_t *next_frontier;
uint32_t next_frontier_size, next_frontier_pos;

uint8_t nbr_threads;
pthread_mutex_t frontier_mtx, next_frontier_mtx, visited_mtx;
pthread_barrier_t barrier;

char done;

void *thread_func(void *args) {
    uint32_t* neighbors;
    uint32_t nbr_neighbors;
    int i;
    do {
        pthread_mutex_lock(&frontier_mtx);
        if (frontier_pos != frontier_size) {
            neighbors = graph[frontier[frontier_pos]].neighbors;
            nbr_neighbors = graph[frontier[frontier_pos]].nbr_neighbors;
            frontier_pos++;
            pthread_mutex_unlock(&frontier_mtx);

            for (i = 0; i < nbr_neighbors; i++) {
                uint32_t neighbor = neighbors[i];
                pthread_mutex_lock(&visited_mtx);
                if (visited[neighbor] == 0) {
                    visited[neighbor] = 1;
                    pthread_mutex_unlock(&visited_mtx);

                    pthread_mutex_lock(&next_frontier_mtx);
                    next_frontier[next_frontier_pos] = neighbor;
                    next_frontier_pos++;
                    if (next_frontier_pos == next_frontier_size) {
                        next_frontier_size *= 2;
                        next_frontier = realloc(next_frontier, sizeof(uint32_t) * next_frontier_size);
                    }
                    pthread_mutex_unlock(&next_frontier_mtx);
                } else {
                    pthread_mutex_unlock(&visited_mtx);
                }
            }
        } else {
            pthread_mutex_unlock(&frontier_mtx);
            int ret = pthread_barrier_wait(&barrier);
            if (ret == PTHREAD_BARRIER_SERIAL_THREAD) {
                if (next_frontier_pos != 0) {
                    uint32_t *temp = next_frontier;
                    uint32_t temp_size = next_frontier_size, temp_pos = next_frontier_pos;
                    next_frontier = frontier;
                    next_frontier_size = frontier_max_size;
                    next_frontier_pos = 0;
                    frontier = temp;
                    frontier_max_size = temp_size;
                    frontier_size = temp_pos;
                    frontier_pos = 0;
                } else {
                    done = 1;
                }
            }
            pthread_barrier_wait(&barrier);
        }
    } while (done == 0);

    return NULL;
}

void bfs(uint32_t nbr_nodes, uint32_t root) {
    frontier_max_size = 100;
    frontier_size = 0;
    frontier_pos = 0;
    frontier = malloc(sizeof(uint32_t) * frontier_max_size);

    next_frontier_size = 100;
    next_frontier_pos = 0;
    next_frontier = malloc(sizeof(uint32_t) * next_frontier_size);

    frontier[0] = root;
    frontier_size = 1;
    visited[root] = 1;

    done = 0;

    pthread_mutex_init(&frontier_mtx, NULL);
    pthread_mutex_init(&visited_mtx, NULL);
    pthread_mutex_init(&next_frontier_mtx, NULL);
    pthread_barrier_init(&barrier, NULL, nbr_threads);

    pthread_t threads[nbr_threads - 1];

    uint8_t i;
    for (i = 0; i < nbr_threads - 1; i++) {
        pthread_create(&threads[i], NULL, &thread_func, NULL);
    }
    thread_func(NULL);

    for (i = 0; i < nbr_threads - 1; i++) {
        pthread_join(threads[i], NULL);
    }

    pthread_mutex_destroy(&frontier_mtx);
    pthread_mutex_destroy(&visited_mtx);
    pthread_mutex_destroy(&next_frontier_mtx);
    pthread_barrier_destroy(&barrier);

    free(frontier);
    free(next_frontier);
}

int main(int argc, char **argv) {
    char *filepath = "graphs/test.txt";
    if (argc > 1) {
        filepath = argv[1];
    }

    uint32_t nbr_nodes;
    printf("Loading graph: %s\n", filepath);
    load_graph(filepath, &graph, &nbr_nodes);
    printf("Done loading graph :)\n");

    struct timespec t_before;
    struct timespec t_after;

    nbr_threads = 1;
    if (argc > 2) {
        nbr_threads = atoi(argv[2]);
    }
    printf("Running bfs with %d threads\n", nbr_threads);
    visited = calloc(nbr_nodes, sizeof(char));

    uint32_t i, j;
    uint64_t exec_times[10];
    for (j = 0; j < 10; j++) {
        exec_times[j] = 0;
    }
    uint64_t mean_exec_time = 0, exec_time, temp;
    for (i = 0; i < 10; i++) {

        // run bfs
        clock_gettime(CLOCK_REALTIME, &t_before);
        bfs(nbr_nodes, 0);
        clock_gettime(CLOCK_REALTIME, &t_after);

        exec_time = (uint64_t)1000000000 * (t_after.tv_sec - t_before.tv_sec) + (t_after.tv_nsec - t_before.tv_nsec);
        mean_exec_time += exec_time;
        printf("%lu ", exec_time);

        temp = 0;
        for (j = 0; j < 10; j++) {
            if (exec_times[j] < exec_time) {
                temp = exec_times[j];
                exec_times[j] = exec_time;
                exec_time = temp;
            }
        }

        for (j = 0; j < nbr_nodes; j++) {
            visited[j] = 0;
        }
    }
    free(visited);
    printf("\n");

    mean_exec_time = mean_exec_time / 10;
    uint64_t median_exec_time = (exec_times[4] + exec_times[5]) / 2;

    printf("Mean: %lu ns\nMedian: %lu ns\nMin: %lu ns\nMax: %lu ns\n", mean_exec_time, median_exec_time, exec_times[9], exec_times[0]);

    /*// run bfs
    clock_gettime(CLOCK_REALTIME, &t_before);
    bfs(nbr_nodes, 0);
    clock_gettime(CLOCK_REALTIME, &t_after);

    printf("Time: %ld s ; %ld ns\n", t_after.tv_sec - t_before.tv_sec, t_after.tv_nsec - t_before.tv_nsec);*/
    free_graph(graph, nbr_nodes);
}
