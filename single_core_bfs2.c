#include "graph_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>

typedef struct llnode {
    uint32_t item;
    struct llnode* next;
} llnode;

void bfs(graphnode* graph, uint32_t nbr_nodes, uint32_t root) {
    char *visited = malloc(nbr_nodes);
    int i;
    for (i = 0; i < nbr_nodes; i++) {
        visited[i] = 0;
    }

    llnode* list_head;
    llnode* list_tail;

    list_head = malloc(sizeof(llnode));
    list_head->item = root;
    list_head->next = NULL;
    list_tail = list_head;
    visited[root] = 1;

    // remove
    /*uint32_t *translation_table = calloc(nbr_nodes, sizeof(uint32_t));
    uint32_t translation_index = 2;
    translation_table[root] = 1;
    uint32_t nbr_visited = 0;
    uint32_t nbr_edges = 0;
    FILE *f = fopen("graphs/almost_good_graph.txt", "w");
    *///

    uint32_t* neighbors;
    uint32_t nbr_neighbors;
    while (list_head != NULL) {
        // remove
        //nbr_visited++;
        //
        //printf("Visiting node %d\n", list_head->item);
        neighbors = graph[list_head->item].neighbors;
        nbr_neighbors = graph[list_head->item].nbr_neighbors;

        for (i = 0; i < nbr_neighbors; i++) {
            uint32_t neighbor = neighbors[i];
            // remove
            /*nbr_edges++;
            if (translation_table[neighbor] == 0) {
                translation_table[neighbor] = translation_index;
                translation_index++;
            }
            fprintf(f, "%d\t%d\n", translation_table[list_head->item] - 1, translation_table[neighbor] - 1);
            *///
            if (visited[neighbor] == 0) {
                visited[neighbor] = 1;
                llnode* n = malloc(sizeof(llnode));
                n->item = neighbor;
                n->next = NULL;
                list_tail->next = n;
                list_tail = n;
            }
        }

        llnode* n = list_head;
        list_head = list_head->next;
        free(n);
    }
    free(visited);
    // remove
    /*free(translation_table);
    printf("nbr_visited: %d\n", nbr_visited);
    fprintf(f, "%d\t%d\n", nbr_visited, nbr_edges);
    fclose(f);
    *///
}

/*void load_graph(char* path, graphnode **graph, uint32_t *nbr_nodes/*///remove///, char undirected*/) {
    /*FILE *f = fopen(path, "r");

    if (f == NULL) {
        printf("Failed to load input file, quitting...\n");
        exit(1);
    }

    uint32_t nodes, nbr_edges;
    fscanf(f, "%d %d", &nodes, &nbr_edges);

    // remove
    //nodes = 400000;
    //

    graphnode *_graph = malloc(sizeof(graphnode) * nodes);
    uint32_t *allocated_memory = malloc(sizeof(uint32_t) * nodes);

    allocated_memory[0] = 0;

    uint32_t i;
    for (i = 0; i < nodes; i++) {
        _graph[i].nbr_neighbors = 0;
        allocated_memory[i] = 10;
        _graph[i].neighbors = malloc(sizeof(uint32_t) * 10);
    }

    uint32_t n1, n2;
    for (i = 0; i < nbr_edges; i++) {
        fscanf(f, "%d %d", &n1, &n2);
        _graph[n1].neighbors[_graph[n1].nbr_neighbors] = n2;
        _graph[n1].nbr_neighbors++;
        if (_graph[n1].nbr_neighbors == allocated_memory[n1]) {
            allocated_memory[n1] *= 2;
            _graph[n1].neighbors = realloc(_graph[n1].neighbors, sizeof(uint32_t) * allocated_memory[n1]);
            if (_graph[n1].neighbors == NULL) {
                printf("Realloc failed, quitting...\n");
                exit(1);
            }
        }
        // remove
        if (undirected != 0) {
            _graph[n2].neighbors[_graph[n2].nbr_neighbors] = n1;
            _graph[n2].nbr_neighbors++;
            if (_graph[n2].nbr_neighbors == allocated_memory[n2]) {
                allocated_memory[n2] *= 2;
                _graph[n2].neighbors = realloc(_graph[n2].neighbors, sizeof(uint32_t) * allocated_memory[n2]);
                if (_graph[n2].neighbors == NULL) {
                    printf("Realloc failed, quitting...\n");
                    exit(1);
                }
            }
        }*/
    /*}

    free(allocated_memory);
    fclose(f);
    *nbr_nodes = nodes;
    *graph = _graph;
}

void free_graph(graphnode *graph, uint32_t nbr_nodes) {
    uint32_t i;
    for (i = 0; i < nbr_nodes; i++) {
        free(graph[i].neighbors);
    }
    free(graph);
}*/

int main(int argc, char **argv) {
    char *filepath = "graphs/test.txt";
    if (argc > 1) {
        filepath = argv[1];
    }

    graphnode* graph;
    uint32_t nbr_nodes;
    printf("Loading graph: %s\n", filepath);
    load_graph(filepath, &graph, &nbr_nodes/*//remove//, 0*/);
    printf("Done loading graph :)\n");

    struct timespec t_before;
    struct timespec t_after;

    // run bfs
    clock_gettime(CLOCK_REALTIME, &t_before);
    bfs(graph, nbr_nodes, 0);
    clock_gettime(CLOCK_REALTIME, &t_after);

    printf("Time: %ld s ; %ld ns\n", t_after.tv_sec - t_before.tv_sec, t_after.tv_nsec - t_before.tv_nsec);
    free_graph(graph, nbr_nodes);
}
