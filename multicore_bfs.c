#include "graph_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>
#include <pthread.h>

typedef struct llnode {
    uint32_t item;
    struct llnode* next;
} llnode;

llnode *frontier_head;
llnode *next_frontier_head, *next_frontier_tail;

graphnode *graph;
char *visited;

uint16_t nbr_threads;

pthread_mutex_t frontier_mtx, next_frontier_mtx, visited_mtx;
//pthread_mutex_t barrier_mtx;
pthread_barrier_t barrier;
//uint16_t threads_in_barrier = 0;
//uint16_t barrier;
//volatile char barrier_open;
char done;

/*void barrier_block() {
    // block until all threads are done, then set frontier to next_frontier
    while (barrier_open);
    pthread_mutex_lock(&barrier_mtx);
    if (barrier == nbr_threads - 1) { // set frontier to next frontier, set done if empty next_frontier
        pthread_mutex_unlock(&barrier_mtx);
        if (next_frontier_head == NULL) {
            done = 1;
        } else {
            frontier_head = next_frontier_head;
            frontier_tail = next_frontier_tail;
            next_frontier_head = NULL;
        }
        if (barrier != 0) {
            barrier_open = 1;
        }
    } else {
        // increment barrier. When leaving opened barrier, decerement it.
        // if barrier is 0 after leaving, close barrier.
        barrier++;
        pthread_mutex_unlock(&barrier_mtx);
        while (barrier_open == 0);
        pthread_mutex_lock(&barrier_mtx);
        barrier--;
        if (barrier == 0) {
            barrier_open = 0;
        }
        pthread_mutex_unlock(&barrier_mtx);
    }
}*/

void *thread_func(void *arg) {
    llnode *crt_node;
    uint32_t i, nbr_neighbors;

    // while not done: get next node and process it
    while (!done) {
        pthread_mutex_lock(&frontier_mtx);
        if (frontier_head != NULL) { // if it exists, get next node in frontier
            crt_node = frontier_head;
            frontier_head = frontier_head->next;
            pthread_mutex_unlock(&frontier_mtx);

            // Add unvisited neighbors to next_frontier
            graphnode g = graph[crt_node->item];
            nbr_neighbors = g.nbr_neighbors;
            for (i = 0; i < nbr_neighbors; i++) {
                uint32_t neighbor = g.neighbors[i];
                pthread_mutex_lock(&visited_mtx);
                if (!visited[neighbor]) { // if neighbor not visited
                    visited[neighbor] = 1; // set as visited
                    pthread_mutex_unlock(&visited_mtx);

                    llnode *n = malloc(sizeof(llnode)); // create llnode for neighbor
                    n->next = NULL;
                    n->item = neighbor;

                    pthread_mutex_lock(&next_frontier_mtx); // add to next_frontier
                    if (next_frontier_head == NULL) {
                        next_frontier_head = n;
                        next_frontier_tail = n;
                    } else {
                        next_frontier_tail->next = n;
                        next_frontier_tail = n;
                    }
                    pthread_mutex_unlock(&next_frontier_mtx);
                } else {
                    pthread_mutex_unlock(&visited_mtx);
                }
            }

            free(crt_node); // done processing current node
        } else { // else go to barrier
            pthread_mutex_unlock(&frontier_mtx);
            //barrier_block();
            int ret = pthread_barrier_wait(&barrier);
            if (ret == PTHREAD_BARRIER_SERIAL_THREAD) {
                if (next_frontier_head != NULL) {
                    llnode *h = next_frontier_head;
                    next_frontier_head = NULL;
                    frontier_head = h;
                } else {
                    done = 1;
                }
            }
            pthread_barrier_wait(&barrier);
        }
    }
    return NULL;
}

void bfs(uint32_t nbr_nodes, uint32_t root) {

    frontier_head = malloc(sizeof(llnode));
    frontier_head->item = root;
    frontier_head->next = NULL;
    next_frontier_head = NULL;

    pthread_mutex_init(&frontier_mtx, NULL);
    pthread_mutex_init(&next_frontier_mtx, NULL);
    pthread_mutex_init(&visited_mtx, NULL);

    pthread_barrier_init(&barrier, NULL, nbr_threads);
    //pthread_mutex_init(&barrier_mtx, NULL);

    //barrier = 0;
    //barrier_open = 0;
    done = 0;

    pthread_t thread_ids[nbr_threads - 1];
    uint16_t i;
    for (i = 0; i < nbr_threads - 1; i++) {
        if (pthread_create(&thread_ids[i], NULL, &thread_func, NULL) != 0) {
            printf("pthread_create failed, quitting...\n");
            exit(0);
        }
    }
    thread_func(NULL);
    for (i = 0; i < nbr_threads - 1; i++) {
        pthread_join(thread_ids[i], NULL);
    }

    pthread_barrier_destroy(&barrier);
    pthread_mutex_destroy(&frontier_mtx);
    pthread_mutex_destroy(&next_frontier_mtx);
    pthread_mutex_destroy(&visited_mtx);
    //pthread_mutex_destroy(&barrier_mtx);
}

int main(int argc, char **argv) {
    char *filepath = "graphs/test.txt";
    if (argc > 1) {
        filepath = argv[1];
    }

    uint32_t nbr_nodes;
    printf("Loading graph: %s\n", filepath);
    load_graph(filepath, &graph, &nbr_nodes);
    printf("Done loading graph :)\n");

    struct timespec t_before;
    struct timespec t_after;

    // set global vars
    nbr_threads = 2;
    visited = calloc(nbr_nodes, sizeof(char));

    // run bfs
    clock_gettime(CLOCK_REALTIME, &t_before);
    bfs(nbr_nodes, 0);
    clock_gettime(CLOCK_REALTIME, &t_after);

    // remove
    uint32_t i, nbr_visited = 0;
    for (i = 0; i < nbr_nodes; i++) {
        if (visited[i]) {
            nbr_visited++;
        }
    }
    printf("Nbr of visited nodes: %d\n", nbr_visited);
    //

    free(visited);

    printf("Time: %ld s ; %ld ns\n", t_after.tv_sec - t_before.tv_sec, t_after.tv_nsec - t_before.tv_nsec);
    free_graph(graph, nbr_nodes);
}
