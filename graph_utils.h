#ifndef GRAPH_UTILS_H_
#define GRAPH_UTILS_H_

#include <stdint.h>

typedef struct graphnode {
    uint32_t nbr_neighbors;
    uint32_t* neighbors;
} graphnode;

void load_graph(char* path, graphnode **graph, uint32_t *nbr_nodes);

void free_graph(graphnode *graph, uint32_t nbr_nodes);

#endif
