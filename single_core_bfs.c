#include "graph_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>

uint32_t *frontier;
uint32_t frontier_size, frontier_pos, frontier_max_size;
uint32_t *next_frontier;
uint32_t next_frontier_size, next_frontier_pos;

char *visited;

void bfs(graphnode* graph, uint32_t nbr_nodes, uint32_t root) {
    frontier_max_size = 100;
    frontier_size = 0;
    frontier_pos = 0;
    frontier = malloc(sizeof(uint32_t) * frontier_max_size);

    next_frontier_size = 100;
    next_frontier_pos = 0;
    next_frontier = malloc(sizeof(uint32_t) * next_frontier_size);

    frontier[0] = root;
    frontier_size = 1;
    visited[root] = 1;

    uint32_t* neighbors;
    uint32_t nbr_neighbors;
    int i;
    do {
        while (frontier_pos != frontier_size) {
            neighbors = graph[frontier[frontier_pos]].neighbors;
            nbr_neighbors = graph[frontier[frontier_pos]].nbr_neighbors;

            for (i = 0; i < nbr_neighbors; i++) {
                uint32_t neighbor = neighbors[i];
                if (visited[neighbor] == 0) {
                    visited[neighbor] = 1;

                    next_frontier[next_frontier_pos] = neighbor;
                    next_frontier_pos++;
                    if (next_frontier_pos == next_frontier_size) {
                        next_frontier_size *= 2;
                        next_frontier = realloc(next_frontier, sizeof(uint32_t) * next_frontier_size);
                    }
                }
            }

            frontier_pos++;
        }
        uint32_t *temp = next_frontier;
        uint32_t temp_size = next_frontier_size, temp_pos = next_frontier_pos;
        next_frontier = frontier;
        next_frontier_size = frontier_max_size;
        next_frontier_pos = 0;
        frontier = temp;
        frontier_max_size = temp_size;
        frontier_size = temp_pos;
        frontier_pos = 0;
    } while (frontier_size != 0);
    free(frontier);
    free(next_frontier);
}

int main(int argc, char **argv) {
    char *filepath = "graphs/test.txt";
    if (argc > 1) {
        filepath = argv[1];
    }

    graphnode* graph;
    uint32_t nbr_nodes;
    printf("Loading graph: %s\n", filepath);
    load_graph(filepath, &graph, &nbr_nodes);
    printf("Done loading graph :)\n");

    struct timespec t_before;
    struct timespec t_after;

    visited = calloc(nbr_nodes, sizeof(char));

    uint32_t i, j;
    uint64_t exec_times[200];
    for (j = 0; j < 200; j++) {
        exec_times[j] = 0;
    }
    uint64_t mean_exec_time = 0, exec_time, temp;
    for (i = 0; i < 200; i++) {

        // run bfs
        clock_gettime(CLOCK_REALTIME, &t_before);
        bfs(graph, nbr_nodes, 0);
        clock_gettime(CLOCK_REALTIME, &t_after);

        exec_time = (uint64_t)1000000000 * (t_after.tv_sec - t_before.tv_sec) + (t_after.tv_nsec - t_before.tv_nsec);
        mean_exec_time += exec_time;
        printf("%lu ", exec_time);

        temp = 0;
        for (j = 0; j < 200; j++) {
            if (exec_times[j] < exec_time) {
                temp = exec_times[j];
                exec_times[j] = exec_time;
                exec_time = temp;
            }
        }

        for (j = 0; j < nbr_nodes; j++) {
            visited[j] = 0;
        }
    }
    free(visited);
    printf("\n");

    mean_exec_time = mean_exec_time / 200;
    uint64_t median_exec_time = (exec_times[99] + exec_times[100]) / 2;

    printf("Mean: %lu ns\nMedian: %lu ns\nMin: %lu ns\nMax: %lu ns\n", mean_exec_time, median_exec_time, exec_times[199], exec_times[0]);

    /*for (i = 0; i < 200; i++) {
        printf("%lu ", exec_times[i]);
    }
    printf("\n");*/

    free_graph(graph, nbr_nodes);
}
