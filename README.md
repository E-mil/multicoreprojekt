### Att göra: ###

* Läs artiklar
    * Kandidater:  
        * ~~A Fast Graph Search Multiprocessor Algorithm (V.Subramaniam, P.-H. Cheng)~~ meh
        * FastForward For Efficient Pipeline Parallelism (J. Giacomoni, T. Moseley, M. Vachharajani)
        * Evaluating Synchronization Techniques for Light-weight Multithreaded/Multicore Architectures (S. Sridharan, A. Rodrigues, P. Kogge)
* implementera parallell bfs
* testa implementationerna
* implementera bättre parallell bfs
* testa igen
* skriv presentation
* skriv rapport

### Gjort: ###

* implementerat sekventiell bfs
* Lästa artiklar:
    * Level-Synchronous Parallel Breadth-First Search Algorithms For Multicore and Multiprocessor Systems  
    https://pdfs.semanticscholar.org/cde0/420a117f8643d066cdcd60c95d5ca39a1082.pdf
    * Topologically Adaptive Parallel Breadth-First Search On Multicore Processors  
    https://pdfs.semanticscholar.org/9cd4/4c4e252ec92d1d39285d2cff241b10e8d727.pdf
    * An Experimental Analysis of a Compact Graph Representation (Skummade)  
    https://pdfs.semanticscholar.org/ad3f/ff3adae0f9c0cd5c927ba5bfe1febf46f64a.pdf
    * Scalable Graph Exploration on Multicore Processors  
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.188.2567&rep=rep1&type=pdf
* Läst lite allmänt om bfs
